# calculator

## Overview
This repository contains the frontend code for my calculator application.
The application uses ReactJS and was written using TypeScript.
<br/>
The application allows users to perform simple calculations.
<br/>
The application is hosted [here](https://calculator.sarahgwalsh.co.uk/) using AWS Amplify.
