/* banner component for the application
contains a menu button and a link to gitlab */

import React from "react";
import { Anchor, Header, Box, Button } from "grommet";
import { Code, Menu } from "grommet-icons";
import { GITLAB_URL } from "../constants/constants";

interface BannerProps {
  showSidebar: boolean;
  setShowSidebar: (value: boolean) => void;
}

const Banner: React.FC<BannerProps> = ({
  setShowSidebar,
  showSidebar,
}): React.ReactElement => {
  return (
    <Header
      elevation="small"
      background="brand"
      pad="medium"
      height="xsmall"
      animation="fadeIn"
      direction="row"
    >
      <Box direction="row" align="center">
        <Button
          onClick={() => setShowSidebar(!showSidebar)}
          icon={<Menu color="white" />}
        ></Button>
      </Box>
      <Anchor
        label="Source Code"
        href={GITLAB_URL}
        color="text-strong"
        icon={<Code />}
      />
    </Header>
  );
};

export default Banner;
